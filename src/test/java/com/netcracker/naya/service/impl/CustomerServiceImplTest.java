package com.netcracker.naya.service.impl;

import com.netcracker.naya.dto.CustomerDto;
import com.netcracker.naya.entity.Address;
import com.netcracker.naya.entity.Customer;
import com.netcracker.naya.service.CustomerService;
import com.netcracker.naya.controller.converter.CustomerConverter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class CustomerServiceImplTest {


    @Autowired
    private CustomerService customerService;

    private Customer testCustomer;

    @BeforeEach
    public void setUp() {

        testCustomer = new Customer();

        testCustomer.setId(new Long(1));
        testCustomer.setName("Wade");
        testCustomer.setSurname("Wilson");
        testCustomer.setEmail("red.psycho@marvel.com");
        testCustomer.setAge(20);

        Address address = new Address();
        address.setCity("NY");
        address.setStreet("Street");
        address.setCustomer(testCustomer);

        testCustomer.setDeliveryAddress(address);
    }


    @Test
    void create() {
        Customer createCustomer = customerService.create(testCustomer);
        assertNotNull(createCustomer.getId());
    }

    @Test
    void findById() {
        customerService.create(testCustomer);
        Customer foundCustomer = customerService.findById(testCustomer.getId());

        assertNotNull(foundCustomer);
    }

    @Test
    void findAll() {
        customerService.create(testCustomer);
        List<Customer> foundCustomers = customerService.findAll();
        assertNotNull(foundCustomers);
    }

    @Test
    void update() {
        Customer customer = customerService.create(testCustomer);
        testCustomer.setName("Steve");
        Customer foundCustomer = customerService.update(testCustomer);
        assertNotEquals(customer.getName(), foundCustomer.getName());
    }

    @Test
    void deleteById() {
        Customer createdCustomer = customerService.create(testCustomer);
        Long id = createdCustomer.getId();
        customerService.deleteById(createdCustomer.getId());
        assertThrows(NullPointerException.class,
                ()->{
                    customerService.findById(id);
                });
    }

}