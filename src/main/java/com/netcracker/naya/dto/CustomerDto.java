package com.netcracker.naya.dto;

import lombok.Data;

import java.util.Random;

@Data
public class CustomerDto implements BaseEntityDto {

    private Long id;

    private String name;

    private String surname;

    private String email;

    private int age;

    private AddressDto deliveryAddress;

    public CustomerDto() {
        id = new Random().nextLong();
    }
}
