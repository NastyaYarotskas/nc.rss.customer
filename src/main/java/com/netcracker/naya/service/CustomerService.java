package com.netcracker.naya.service;

import com.netcracker.naya.dto.CustomerDto;
import com.netcracker.naya.entity.Customer;

import java.util.List;

public interface CustomerService {

    Customer create(Customer customer);

    Customer findById(Long id);

    List<Customer> findAll();

    Customer update(Customer customer);

    void deleteById(Long id);

}

