package com.netcracker.naya.service.impl;

import com.netcracker.naya.dto.CustomerDto;
import com.netcracker.naya.entity.Customer;
import com.netcracker.naya.exception.EntityNotFoundException;
import com.netcracker.naya.repository.CustomerRepository;
import com.netcracker.naya.service.CustomerService;
import com.netcracker.naya.controller.converter.CustomerConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;

    @Autowired
    public CustomerServiceImpl(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public Customer create(Customer customer) {
        return customerRepository.save(customer);
    }

    @Override
    public Customer findById(Long id) {
        Customer customer = customerRepository.findById(id).orElseThrow(NullPointerException::new);
        return customer;
    }

    @Override
    public List<Customer> findAll() {
        return customerRepository.findAll();
    }

    @Override
    public Customer update(Customer customer) {
        return customerRepository.save(customer);
    }

    @Override
    public void deleteById(Long id) {
        try {
            customerRepository.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            throw new EntityNotFoundException(e.getMessage());
        }
    }

}

