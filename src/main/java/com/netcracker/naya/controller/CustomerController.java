package com.netcracker.naya.controller;

import com.netcracker.naya.dto.CustomerDto;
import com.netcracker.naya.entity.Customer;
import com.netcracker.naya.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.netcracker.naya.controller.converter.*;

import java.util.List;

@RestController
@RequestMapping(value = "api/v1/order-entry/customers")
public class CustomerController {

    private final CustomerService customerService;

    private final CustomerConverter customerConverter;

    @Autowired
    public CustomerController(CustomerService customerService, CustomerConverter customerConverter) {
        this.customerService = customerService;
        this.customerConverter = customerConverter;
    }

    @GetMapping(path = "/{id}")
    public CustomerDto findById(@PathVariable("id") Long id) {
        return customerConverter.convertFromEntity(customerService.findById(id));
    }

    @GetMapping
    public List<CustomerDto> findAll() {
        return customerConverter.createFromEntities(customerService.findAll());
    }

    @PostMapping
    public CustomerDto create(@RequestBody @Validated CustomerDto customerDto) {
        Customer customer = customerConverter.convertFromDto(customerDto);
        customer = customerService.create(customer);
        return customerConverter.convertFromEntity(customer);
    }

    @PutMapping
    public CustomerDto update(@RequestBody @Validated CustomerDto customerDto) {
        Customer customer = customerConverter.convertFromDto(customerDto);
        customer = customerService.create(customer);
        return customerConverter.convertFromEntity(customerService.update(customer));
    }

    @DeleteMapping(path = "/{id}")
    public void deleteById(@PathVariable("id") Long id) {
        customerService.deleteById(id);
    }

}

