package com.netcracker.naya.controller.converter;

import com.netcracker.naya.dto.CustomerDto;
import com.netcracker.naya.entity.Customer;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.stereotype.Component;

@Component
public class ModelMapperConverter implements CustomerConverter {

    private ModelMapper modelMapper;

    public ModelMapperConverter() {
        modelMapper = new ModelMapper();

        modelMapper.addMappings(new PropertyMap<CustomerDto, Customer> () {
            @Override
            protected void configure() {
                map().setName(source.getName());
            }
        });

        modelMapper.addMappings(new PropertyMap<Customer, CustomerDto> () {
            @Override
            protected void configure() {
                map().setName(source.getName());
            }
        });
    }

    @Override
    public Customer convertFromDto(CustomerDto customerDto) {
        return modelMapper.map(customerDto, Customer.class);
    }

    @Override
    public CustomerDto convertFromEntity(Customer customer) {
        return modelMapper.map(customer, CustomerDto.class);
    }
}
