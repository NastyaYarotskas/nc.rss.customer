package com.netcracker.naya.entity;

import com.netcracker.naya.dto.CustomerDto;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.*;
import java.util.Objects;

@Document
@Data
public class Customer extends BaseEntity{

    private String name;

    private String surname;

    private String email;

    private int age;

    @OneToOne(mappedBy = "customer", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Address deliveryAddress;

    public Customer() {
    }

    public Customer(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public Customer(String name, String surname, String email, int age) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.age = age;
    }

    private Customer(CustomerDto customerDto) {
        this.name = customerDto.getName();
        this.surname = customerDto.getSurname();
        this.email = customerDto.getEmail();
        this.age = customerDto.getAge();
    }

    public static Customer convertFromDto(CustomerDto customerDto) {
        return new Customer(customerDto);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return Objects.equals(name, customer.name)
                && Objects.equals(surname, customer.surname)
                && Objects.equals(age, customer.age);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), name, surname, age);
    }


    @Override
    public String toString() {
        return "Customer{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", email='" + email + '\'' +
                ", age=" + age +
                ", deliveryAddress city=" + deliveryAddress.getCity() +
                '}';
    }
}

